<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="fas fa-user-circle f25"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <a class="dropdown-item" href="/user/list/{{Auth::user()->id}}/edit"> Change Password </a>
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</nav>
<!-- /.navbar -->
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/dashboard" class="brand-link">
        <img src="/logo/logo.png" alt="NIMOL Accessories & Hardware LOGO" class="brand-image img-circle" style="opacity: .8">
        <span class="brand-text font-weight-light">GRAND222</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <img src="/logo/logo.png" class="img-circle" alt="User Image">
        </div>
        <div class="info">
            <a href="#" class="d-block"> 
                @if (Auth::user()) {{Auth::user()->name}} @endif 
            </a>
        </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="/dashboard" class="nav-link {{ (request()->is('dashboard')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>ទំព័រដើម</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/user/list" class="nav-link {{ (request()->is('/user/list')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>គ្រប់គ្រងភ្ញៀវ</p>
                    </a>
                </li>
                <li class="nav-header">SETTINGS</li>
                <li class="nav-item has-treeview {{ (request()->is('bank', 'bank/create', 'bank/*/edit', 'bank/*/detail')) ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ (request()->is('bank', 'bank/create', 'bank/*/edit', 'bank/*/detail')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-shopping-cart"></i>
                        <p> Bank <i class="right fas fa-angle-left"></i> </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/bank/create" class="nav-link {{ (request()->is('bank/create', 'bank/*/edit')) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i><p>បង្កើតថ្មី</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/bank" class="nav-link {{ (request()->is('bank', 'bank/*/detail')) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i><p>បញ្ជីទិន្នន័យ</p>
                            </a>
                        </li>
                    </ul>
                </li>
                
            </ul>
        </nav>
    </div>
</aside>