@extends('layouts.app')

@section('content')
    <div class="container">
        @if (Auth::user()) 
        <div class="card mg-t50 mg-b50">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2>ขอบคุณสำหรับการลงทะเบียน </h2>
                        <br>
                        <h3>เราจะติดต่อคุณเร็ว ๆ นี้</h3>
                    </div>
                </div>
            </div>
        </div>
        @else
            <div class="card mg-t50 mg-b50">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <br><br>
                            <a href="/login" class="btn btn-info btn-block">เข้าสู่ระบบ</a>
                            <br><br>
                        </div>
                        <div class="col-md-6">
                            <br><br>
                            <a href="/register" class="btn btn-primary btn-block">สมัครสมาชิก</a>
                            <br><br>
                        </div>
                    </div>
                </div>
            </div>
        @endif 
        
    </div>

@endsection