@extends('layouts.appdashboard')

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">ផ្ទាំងគ្រប់គ្រង</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/dashboard">ទំព័រដើម</a></li>
                            <li class="breadcrumb-item active">ផ្ទាំងគ្រប់គ្រង</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="container-fluid">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="info-box">
							<span class="info-box-icon bg-info"><i class="fas fa-shopping-cart"></i></span>
							<div class="info-box-content">
								<span class="info-box-text">ភ្ញៀវ</span>
								<span class="info-box-number">
									<span class="small"> ចំនួន </span> {{$totalUser}}
								</span>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="info-box">
							<span class="info-box-icon bg-warning"><i class="fa fa-suitcase"></i></span>
							<div class="info-box-content">
								<span class="info-box-text">ធនាគារ</span>
								<span class="info-box-number">
									<span class="small"> ចំនួន </span> {{$totalBank}}
								</span>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
	</div>

@endsection

