@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="mg-t50"></div>
            <div class="card bg-yello-old">
                {{-- <div class="card-header">{{ __('Register') }}</div> --}}

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('ชื่อจริง') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('หมายเลขโทรศัพท์') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="phone" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone">

                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="bank_number" class="col-md-4 col-form-label text-md-right">{{ __('หมายเลขบัญชี') }}</label>

                            <div class="col-md-6">
                                <input id="bank_number" type="bank_number" class="form-control @error('bank_number') is-invalid @enderror" name="bank_number" value="{{ old('bank_number') }}" required autocomplete="bank_number">

                                @error('bank_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">

                            <label for="bank_id" class="col-md-4 col-form-label text-md-right">{{ __('เลือกธนาคาร') }}</label>
                            <div class="col-md-6">
                                {{Form::select('bank_id', $selectBank, old('bank_id'), ['class' => 'form-control'])}}
                                @error('bank_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            {{-- <label for="bank_id" class="col-md-4 col-form-label text-md-right">{{ __('เลือกธนาคาร') }}</label> --}}

                            {{-- <div class="col-md-6">
                                <select class="form-control select2" name="bank_id">
                                    <option value="">---เลือกธนาคาร---</option>
                                    @if (count($adminUser) > 0)
                                        @foreach ($adminUser as $itm)
                                            <option value="{{$itm->id}}" @if (Request::get('userid') == $itm->id) selected @endif>{{$itm->name}}</option>
                                        @endforeach
                                    @endif
                                </select>

                                
                            </div> --}}
                        </div>

                        <div class="form-group row">
                            <label for="lineid" class="col-md-4 col-form-label text-md-right">{{ __('ไลน์ไอดี') }}</label>

                            <div class="col-md-6">
                                <input id="lineid" type="lineid" class="form-control @error('lineid') is-invalid @enderror" name="lineid" value="{{ old('lineid') }}" required autocomplete="lineid">

                                @error('lineid')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('รหัสผ่าน') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('ยืนยันรหัสผ่าน') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fas fa-user-plus"></i> {{ __('สมัครสมาชิก') }}
                                </button>
                                <a href="https://line.me/R/ti/p/%40grand222" class="btn btn-info" target="_blank"><i class="fab fa-line"></i> ติดต่อเรา</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="mg-t50"></div>
        </div>
    </div>
</div>
@endsection
