@extends('layouts.appdashboard')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark"> ឈ្មោះធនាគារ </h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/dashboard">ទំព័រដើម</a></li>
                            <li class="breadcrumb-item"><a href="#">ឈ្មោះធនាគារ</a></li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h5 class="card-title m-0">បញ្ជីទិន្នន័យ</h5>
                            </div>
                            <div class="card-body">
                                @if (session('error'))
                                    <div class="alert alert-danger">
                                        {{session('error')}}
                                    </div>
                                @endif
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>                  
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>ឈ្មោះ</th>
                                            <th style="width: 100px;">កែប្រែ</th>
                                            <th style="width: 100px;">លុប</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @if (count($datas) > 0)
                                                @foreach ($datas as $item)
                                                <tr>
                                                    <td>{{$item->vieworder}}</td>
                                                    <td>{{$item->name}}</td>
                                                    <td>
                                                        <a href="/bank/{{$item->id}}/edit" class="btn btn-info btn-sm"><i class="far fa-edit"></i> កែប្រែ</button>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-delete{{$item->id}}"><i class="far fa-trash-alt"></i> លុប</button>
                                                        <div class="modal fade" id="modal-delete{{$item->id}}">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                    <h4 class="modal-title">បញ្ជាក់</h4>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                    <p>តើអ្នកប្រាកដជាលុបទិន្នន័យនេះឬ?</p>
                                                                    </div>
                                                                    <div class="modal-footer justify-content-between">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">មិនយល់ព្រម</button>

                                                                        {!! Form::open(['action' => ['BankController@destroy', $item->id], 'method' => 'POST']) !!}
                                                                            {{Form::hidden('_method', 'DELETE')}}
                                                                            {{Form::submit('យល់ព្រម', ['class' => 'btn btn-danger'])}}
                                                                        {!! Form::close() !!}
                                                                    </div>
                                                                </div>
                                                                <!-- /.modal-content -->
                                                            </div>
                                                            <!-- /.modal-dialog -->
                                                        </div>
                                                        <!-- /.modal -->
                                                    </td>
                                                </tr>
                                                @endforeach
                                            @else
                                            <tr>
                                                <td colspan="4" class="text-center">មិនមានទិន្នន័យទេ</td>
                                            </tr>
                                            @endif
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer clearfix">
                                {{$datas->links()}}
                            </div>
                        </div>
                    </div>
                    <!-- /.col-md-6 -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection