@extends('layouts.appdashboard')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark"> ឈ្មោះធនាគារ </h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/dashboard">ទំព័រដើម</a></li>
                            <li class="breadcrumb-item"><a href="#">ឈ្មោះធនាគារ</a></li>
                            <li class="breadcrumb-item active">បង្កើតថ្មី</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-danger">
                            <div class="card-header">
                              <h3 class="card-title">ទំម្រង់បញ្ចូលទិន្នន័យ</h3>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['action' => 'BankController@store', 'method' => 'POST', 'id' => 'validateForm']) !!}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {{Form::label('name', 'ឈ្មោះ')}} <span class="text-danger">*</span>
                                                {{Form::text('name', '', ['class' => 'form-control'])}}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {{Form::label('vieworder', 'លេខរៀង')}} <span class="text-danger">*</span>
                                                {{Form::number('vieworder', '', ['class' => 'form-control'])}}
                                            </div>
                                        </div>
                                    </div>

                                    {{Form::submit('រក្សាទុក', ['class' => 'btn btn-primary'])}}
                                {!! Form::close() !!}
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $.validator.setDefaults({
                submitHandler: function () {
                    return true;
                }
            });
            $('#validateForm').validate({
                rules: {
                    name: {
                        required: true,
                    },
                    vieworder: {
                        required: true,
                    },
                },
                messages: {
                    name: {
                        required: "សូមបញ្ចូលឈ្មោះ",
                    },
                    vieworder: {
                        required: "សូមបញ្ចូលលេខរៀង",
                    }
                },
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });
    </script>
@endsection