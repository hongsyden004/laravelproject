<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    {{-- <meta charset="utf-8"> --}}
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
  
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    {{-- <link rel="icon" href="/logo/logo200x200.png" type="image/gif" sizes="16x16"> --}}
    <link rel="icon" type="image/png" sizes="96x96" href="/public/logo/favicon-96x96.png">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('themes/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('themes/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{ asset('themes/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('themes/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('themes/dist/css/adminlte.css') }}">
     <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('themes/plugins/summernote/summernote-bs4.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Hanuman&display=swap" rel="stylesheet">
    
    <link rel="stylesheet" href="{{ asset('custom.css') }}">
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
    
<div class="wrapper">

    @include('inc.navbaruser')
    
    @yield('content')

    <!-- Main Footer -->
    <footer class="main-footer">
		<div class="container">
			<strong>Copyright &copy;2020 GCLUB557.</strong> All rights reserved.
		</div>
    </footer>
    <div class="loader"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{ asset('themes/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('themes/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('themes/plugins/select2/js/select2.full.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('themes/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- jquery-validation -->
<script src="{{ asset('themes/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('themes/plugins/jquery-validation/additional-methods.min.js') }}"></script>

<!-- InputMask -->
<script src="{{ asset('themes/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('themes/plugins/inputmask/min/jquery.inputmask.bundle.min.js') }}"></script>

<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('themes/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>

<!-- AdminLTE App -->
<script src="{{ asset('themes/dist/js/adminlte.js') }}"></script>

<!-- OPTIONAL SCRIPTS -->
{{-- <script src="{{ asset('themes/dist/js/demo.js') }}"></script> --}}

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{ asset('themes/plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('themes/plugins/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('themes/plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
<script src="{{ asset('themes/plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('themes/plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('themes/plugins/chart.js/Chart.min.js') }}"></script>

<!-- PAGE SCRIPTS -->
{{-- <script src="{{ asset('themes/dist/js/pages/dashboard2.js') }}"></script> --}}

<script>
    $(function () {
		// Summernote
		$('.texteditor').summernote({
			height: 150
		})
    })
</script>
  
@yield('javascript')

</body>
</html>