@extends('layouts.appdashboard')

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">គ្រប់គ្រងភ្ញៀវ</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/dashboard">ទំព័រដើម</a></li>
                            <li class="breadcrumb-item active">គ្រប់គ្រងភ្ញៀវ</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main content -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <form action="/user/list">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="kwd" value="{{ Request::get('kwd') }}" placeholder="ពាក្យគន្លឹះ">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary btn-block">ស្វែងរក</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h5 class="card-title m-0">បញ្ជីទិន្នន័យ</h5>
                            </div>
                            <div class="card-body">
                                @if (session('error'))
                                    <div class="alert alert-danger">
                                        {{session('error')}}
                                    </div>
                                @endif
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>                  
                                        <tr>
                                            <th>ឈ្មោះ</th>
                                            <th>លេខទូរស័ព្ទ</th>
                                            <th>ឈ្មោះធនាគារ</th>
                                            <th>លេខគណនីធនាគារ</th>
                                            <th>LINE ID</th>
                                            <th>បង្កើត Account</th>
                                            <th style="width: 150px;">ផ្លាស់ប្តូរ</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @if (count($datas) > 0)
                                                @foreach ($datas as $item)
                                                <tr>
                                                    <td>{{$item->name}}</td>
                                                    <td>{{$item->phone}}</td>
                                                    <td>{{$item->bank->name}}</td>
                                                    <td>{{$item->bank_number}}</td>
                                                    <td>
                                                        @php
                                                            $rsLine = str_replace('@', '', $item->lineid);
                                                            echo "<a href='https://line.me/R/ti/p/@$rsLine' target='_blank'>$item->lineid</a>";
                                                        @endphp
                                                    </td>
                                                    <td>
                                                        @if ($item->is_created === 1)
                                                            <span class="badge badge-warning"><i class="fas fa-ban"></i> មិនទាន់បង្កើត</span>
                                                        @else
                                                            <span class="badge badge-success"><i class="far fa-check-circle"></i> បង្កើតរួចហើយ</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modal-iscreate{{$item->id}}"><i class="fas fa-user-plus"></i> ប្តូរ</button>
                                                        <div class="modal fade" id="modal-iscreate{{$item->id}}">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                    <h4 class="modal-title">បញ្ជាក់</h4>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                    <p>តើភ្ញៀវនេះពិតជាបានបង្កើតរួចហើយ ឬមែនទេ?</p>
                                                                    </div>
                                                                    <div class="modal-footer justify-content-between">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">បិទ</button>

                                                                        {!! Form::open(['action' => ['UsermanageController@update', $item->id], 'method' => 'POST']) !!}
                                                                            {{Form::hidden('is_created', 2, ['class' => 'form-control'])}}
                                                                            {{Form::hidden('_method', 'PUT')}}
                                                                            {{Form::submit('យល់ព្រម', ['class' => 'btn btn-info'])}}
                                                                        {!! Form::close() !!}
                                                                    </div>
                                                                </div>
                                                                <!-- /.modal-content -->
                                                            </div>
                                                            <!-- /.modal-dialog -->
                                                        </div>
                                                        <!-- /.modal -->
                                                        @if (Auth::user()->role == 2)
                                                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modal-delete{{$item->id}}"><i class="far fa-trash-alt"></i> លុប</button>
                                                        <div class="modal fade" id="modal-delete{{$item->id}}">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                    <h4 class="modal-title">បញ្ជាក់</h4>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                    <p>តើពិតជាចង់លុបភ្ញៀវនេះ ឬមែនទេ?</p>
                                                                    </div>
                                                                    <div class="modal-footer justify-content-between">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">បិទ</button>

                                                                        {!! Form::open(['action' => ['UsermanageController@destroy', $item->id], 'method' => 'POST']) !!}
                                                                            {{Form::hidden('_method', 'DELETE')}}
                                                                            {{Form::submit('យល់ព្រម', ['class' => 'btn btn-info'])}}
                                                                        {!! Form::close() !!}
                                                                    </div>
                                                                </div>
                                                                <!-- /.modal-content -->
                                                            </div>
                                                            <!-- /.modal-dialog -->
                                                        </div>
                                                        <!-- /.modal -->
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            @else
                                            <tr>
                                                <td colspan="7" class="text-center">មិនមានទិន្នន័យទេ</td>
                                            </tr>
                                            @endif
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer clearfix">
                                {{$datas->links()}}
                            </div>
                        </div>
                    </div>
                    <!-- /.col-md-6 -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
	</div>

@endsection

