@extends('layouts.appdashboard')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark"> កែប្រែ Password </h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/dashboard">ទំព័រដើម</a></li>
                            <li class="breadcrumb-item"><a href="#">Password</a></li>
                            <li class="breadcrumb-item active">កែប្រែ</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-danger">
                            <div class="card-header">
                              <h3 class="card-title">ទំម្រង់បញ្ចូលទិន្នន័យ</h3>
                            </div>
                            <div class="card-body">
                                {!! Form::open(['action' => ['UsermanageController@update', Auth::user()->id], 'method' => 'POST', 'id' => 'validateForm']) !!}
                                    <div class="row">
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                {{Form::label('password', 'Password')}} <span class="text-danger">*</span>
                                                {{Form::text('password', '', ['class' => 'form-control'])}}
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            {{Form::hidden('_method', 'PUT')}}
                                            {{Form::hidden('is_pwd', 1, ['class' => 'form-control'])}}
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                {{Form::submit('Change', ['class' => 'btn btn-primary form-control'])}}
                                            </div>
                                            
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $.validator.setDefaults({
                submitHandler: function () {
                    return true;
                }
            });
            $('#validateForm').validate({
                rules: {
                    password: {
                        required: true,
                        minlength: 8
                    },
                },
                messages: {
                    password: {
                        required: "សូមបញ្ចូល Password",
                        minlength: "Password ត្រូវ៨ខ្ទង់"
                    }
                },
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });
    </script>
@endsection