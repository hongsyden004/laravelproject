<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('bank_id');
            $table->integer('role')->default(1)->comment('1=User, 2=Admin, 3=Staff');
            $table->string('bank_number');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone');
            $table->string('lineid');
            $table->integer('is_created')->default(1)->comment('1=Not Create Acc, 2=Has Create Acc (For Other account)');
            $table->date('is_deleted')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
