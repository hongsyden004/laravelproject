<?php

namespace App\Http\Controllers;
use App\Bank;
use App\User;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
   /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(auth()->user()->role == 1) {
            return redirect('/page/error');
        }
        $totalBank = Bank::all()->count();
        $totalUser = User::where('role', '=', '1')->whereNull('is_deleted')->count();

        $data = array('totalBank' => $totalBank, 'totalUser' => $totalUser);

        return view('dashboard.index')->with($data);
    }
}
