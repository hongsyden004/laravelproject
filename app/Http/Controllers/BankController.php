<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bank;
use App\User;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check for correct user
        if(auth()->user()->role == 1) {
            return redirect('/page/error');
        }

        $rsBank = Bank::orderBy('vieworder', 'asc')->paginate(10);
        return view('bank.index')->with('datas', $rsBank);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Check for correct user
        if(auth()->user()->role == 1) {
            return redirect('/page/error');
        }

        return view('bank.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Check for correct user
        if(auth()->user()->role == 1) {
            return redirect('/page/error');
        }

        $this->validate($request, [
            'name' => 'required',
            'vieworder' => 'required'
        ]);
        
        $collection = new Bank;
        $collection->name = $request->input('name');
        $collection->vieworder = $request->input('vieworder');
        $collection->save();
        return redirect('/bank')->with('success', 'ទិន្នន័យត្រូវបានរក្សាទុក');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Check for correct user
        if(auth()->user()->role == 1) {
            return redirect('/page/error');
        }

        $collection = Bank::find($id);
        return view('bank.edit')->with('data', $collection);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Check for correct user
        if(auth()->user()->role == 1) {
            return redirect('/page/error');
        }

        $this->validate($request, [
            'name' => 'required',
            'vieworder' => 'required'
        ]);
        
        $collection = Bank::find($id);
        $collection->name = $request->input('name');
        $collection->vieworder = $request->input('vieworder');
        $collection->save();
        return redirect('/bank')->with('success', 'ទិន្នន័យត្រូវបានរក្សាទុក');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Check for correct user
        if(auth()->user()->role == 1) {
            return redirect('/page/error');
        }

        $total = User::where('bank_id', $id)->count();
        $collection = Bank::find($id);

        if($total == 0)
        {
            $collection->delete();
            return redirect('/bank');
        } else {
            return redirect('/bank')->with('error', 'ឈ្មោះធនាគារ '.$collection->name.' បានប្រើប្រាស់ហើយដូច្នេះមិនអាចលុបបានទេ!!!');
        }
    }
}
