<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsermanageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check for correct user
        if(auth()->user()->role == 1) {
            return redirect('/page/error');
        }

        $rsUnit = User::where('role', '=', 1)->whereNull('is_deleted')->orderBy('id', 'DESC')->paginate(10);
        return view('usermanage.index')->with('datas', $rsUnit);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Check for correct user
        if(auth()->user()->role == 1) {
            return redirect('/page/error');
        }

        $collection = User::find($id);
        return view('usermanage.edit')->with('data', $collection);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Check for correct user
        if(auth()->user()->role == 1) {
            return redirect('/page/error');
        }
        $collection = User::find($id);

        if($request->input('is_pwd')) 
        {
            $this->validate($request, [
                'password' => 'required'
            ]);
            $collection->password = Hash::make($request->input('password'));
            $collection->save();
            return redirect('/dashboard')->with('success', 'ទិន្នន័យត្រូវបានរក្សាទុក');
        } else {
            $this->validate($request, [
                'is_created' => 'required'
            ]);
            $collection->is_created = $request->input('is_created');
            $collection->save();
            return redirect('/user/list')->with('success', 'ទិន្នន័យត្រូវបានរក្សាទុក');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Check for correct user
        if(auth()->user()->role == 1) {
            return redirect('/page/error');
        }
        
        $collection = User::find($id);
        $collection->is_deleted = date("Y-m-d");
        $collection->save();
        return redirect('/user/list')->with('success', 'ទិន្នន័យត្រូវបានរក្សាទុក');
    }
}
