<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    //Table name
    protected $table = 'bank';
    // primary key
    public $primaryKey = 'id';
    // TimeStamps
    public $timestamps = true;

    public function user() {
        return $this->hasMany('App\User');
    }

}
